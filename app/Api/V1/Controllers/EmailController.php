<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Classes\MailHelper;

class EmailController
{
  private $mail_helper;
  /**
    * Create a new controller instance.
    *
    * @return void
    */
  public function __construct()
  {
    $this->mail_helper = new MailHelper();
  }

  public function send_tac(Request $request)
  { 
    $req = $request->all();
    $rules = [
      'to_email'       => ['required', 'email'],  
      'customer_name'  => ['required'],
      'tac'            => ['required'],
      'expiry_time'    => ['required'],
    ];

    $validator = app('validator')->make($req, $rules);
    if ($validator->fails()) {
      return $this->mail_helper->response(92002, $validator->errors());
    }  

    $req['headline']      = "Thank you for registering with us!";
    $req['body']          = "We have create an account for you. All you need to do is verify this email by using the code below: <br />
      <br />" . $req['tac'] . "<br /><br />
      The code will expire in " . ($req['expiry_time'] / 60)." minutes.<br/><br/>
      Please click this link to proceed with verification: <br />
      " . env('FASTPORT_VERIFY_PAGE') . "<br /><br /> Happy trip!";
    $subject              = "Your Fastport account needs verification";
    $email_template_path  = env('EMAIL_TAC_TEMPLATE');
    $email_data           = $this->mail_helper->send_email($subject, $req, $email_template_path);   
    return $email_data;
  }
}
