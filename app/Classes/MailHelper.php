<?php
namespace App\Classes;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class MailHelper {

  public function send_email($subject, $content, $email_template_path, $attachment='')
  {
    if($attachment != ''){
      Mail::send($email_template_path, $content, function($message) use ($content, $subject, $attachment) {
        $message->from(env('NOREPLY_EMAIL'))
                ->to($content['to_email'])
                ->attach($attachment)
                ->subject($subject);
        });
    } else {
      Mail::send($email_template_path, $content, function($message) use ($content, $subject) {
        $message->from(env('NOREPLY_EMAIL'))
                ->to($content['to_email'])
                ->subject($subject);
        });
    }
      
    if(Mail::failures()) {
        return $this->response(400, 'Email failed');
    } else {
        return $this->response(200, 'Email sent successfully');
    }
  }

  public function response($code, $response)
  {
    $now = new Carbon;
    if($code == 200) {
      $result = [
        'code' => $code,
        'status' => 'Success',
        'timestamp' => $now->timestamp,
        'response' => $response
      ];
    }
    else {
      $result = [
        'code' => $code,
        'status' => 'Failure',
        'timestamp' => $now->timestamp,
        'errorMsg' => $response
      ];
    }

    return $result;
  }
}