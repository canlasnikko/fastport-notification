<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
  'prefix' => 'email'
], function ($api)
{
  $api->post('/tac', ['uses' => 'App\Api\V1\Controllers\EmailController@send_tac']);
});
