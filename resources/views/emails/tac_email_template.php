<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>unifi</title>
<style>
h4 {
  font-size:18pt;
  color:#005aa9;
  font-weight:bold; 
}


</style>
</head>

<body style="margin:0">
<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td><table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
      </table>

      <table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td height="30" align="center" valign="middle" bgcolor="#ffffff"><img src="<?php echo $message->embed(env('FASTPORT_GW'). '/storage/app/media/project_images/fastport_image.png');?>" alt="" width="600" /></td>
        </tr>
      </table>
      
      <table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td align="center" valign="middle" bgcolor="#ffffff" style="padding:20px 20px; color:#000000; font-size:14px; line-height:20px; font-family:'Lato', Arial, Helvetica, sans-serif; text-align:left;"> 
          
          <?php if($headline!==''){
              ?>
            <br />
              <?php echo $headline;?>
            
              <?php
          }?>
          <p></p>
          Hello <?php echo $customer_name;?>,

<br /><br />

<?php echo $body;?>
<br />

                
          </td>
        </tr>
      </table>
<br /><br /><p></p>
</body>
</html>